const axios = require('axios');
const { validateToken } = require('../api');

const validateJwt = jwt => {
  return axios({
    url: validateToken,
    method: 'get',
    headers: {
      'Accept': 'application/json',
      'Authorization': `Bearer ${jwt}`
    }
  }).then(res => {
    if(res.data.status === 'success') {
      return 200;
    }
    return 401;
  });
};

module.exports = validateJwt;
