const asyncLimit = (fn, n) => {
  let pendingPromises = [];
  return async function (...args) {
    while (pendingPromises.length >= n) {
      // console.log('RACE ' + pendingPromises.length);
      await Promise.race(pendingPromises).catch(() => {});
    }
    // console.log('NORMAL ' + pendingPromises.length);
    const p = fn.apply(this, args);
    pendingPromises.push(p);
    await p.catch(() => {});
    pendingPromises = pendingPromises.filter(pending => pending !== p);
    return p;
  };
};

module.exports = asyncLimit;