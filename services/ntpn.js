const axios = require('axios');
const fs = require('fs');
const querystring = require('querystring');
const asyncLimit = require('./async-limit');
const { konfirmasi } = require('../api');
const redisClient = require('./redis');

function Ntpn(request) {
  this.fetchNtpn = (fileName) => {
    const ASYNC_LIMIT = 9;

    let promisedNtpn = [];
    let limitedDoKonfirmasi = asyncLimit(doKonfirmasi, ASYNC_LIMIT);

    let billings = request.billings.split(',');
    billings.forEach(element => {
      let fetchedContent = limitedDoKonfirmasi(element);
      promisedNtpn.push(fetchedContent);
    });
  
    return Promise.all(promisedNtpn).then(penerimaanObjects => {
      this.writeNtpn(penerimaanObjects, fileName);
    });
  }

  this.writeNtpn = (penerimaanObjects, fileName) => {
    let tabDelimited = "BILLING \t AKUN \t NTPN \t JUMLAH \n";

    penerimaanObjects.forEach(element => {
      tabDelimited = tabDelimited +"'"+ element[0] +'\t'+ element[1] +'\t'+element[2] +'\t'+ element[3] +'\n';
    });

    const filePath = `./public/${fileName}.xls`;

    return fs.writeFile(filePath, tabDelimited, err => {
      if (err) throw err;
      redisClient.set('rekon:' + fileName, true);
    });
  }

  const doKonfirmasi = billingCode => {
    return axios({
      url: konfirmasi,
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${request.jwt}`
      },
      data: querystring.stringify({
        'submit_file': '',
        'nontpn': '',
        'nobillingcode': billingCode,
        'akun': ''
      })
    }).then(res => {
      return parseKonfirmasi(res.data.data.body[0]);
    }).catch(err => {
      // console.log(err);
      return [billingCode, null, null, null];
    });
  }

  const parseKonfirmasi = dataPenerimaan => {
    let ntpn = [];
    const billingNtpn = dataPenerimaan[1].value.split('<br>');

    ntpn[0] = billingNtpn[1];
    ntpn[1] = dataPenerimaan[6].value.split('.')[2];
    ntpn[2] = billingNtpn[0];
    ntpn[3] = dataPenerimaan[9].value.replace(/,/g , '');
// console.log(ntpn);
    return ntpn;
  }
}

module.exports = Ntpn;