const auth = require('./auth');
const ntpn = require('./ntpn');
const retrieve = require('./retrieve');

module.exports = {
  auth,
  ntpn,
  retrieve
}