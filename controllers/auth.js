process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const express = require('express');
const axios = require('axios');
const querystring = require('querystring');
const { auth } = require('../api');

const router = express.Router();

router.post('/', (request, response) => {
  return axios({
    url: auth,
    method: 'post',
    headers: {
      'Accept': 'application/json'
    },
    data: querystring.stringify(request.body)
  }).then(res => {
    return response.status(200).send(res.data);
  }).catch(err => {
    return response.status(404).send({ msg: err.message });
  });
});

module.exports = router;