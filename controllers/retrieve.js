const express = require('express');
const redisClient = require('../services/redis');
const { promisify } = require('util');
const getAsync = promisify(redisClient.get).bind(redisClient);
const validateJwt = require('../services/validate-jwt');

const router = express.Router();

router.get('/', async (request, response) => {
  const requestPayloads = {
    url: request.originalUrl,
    jwt: request.headers.authorization,
    file: request.query.file,
    download: request.query.download
  }

  try {
    const fileExist = await getAsync('rekon:' + requestPayloads.file);

    if(fileExist) {
      if(requestPayloads.download === 'true') {
        return response.status(200).download('./public/' + requestPayloads.file + '.xls');
      }

      return response.status(200).send({
        file: requestPayloads.url
      });
    }

    return response.status(503).send({
      file: null
    });
  } catch (error) {
    return response.status(503).send({
      file: null
    });
  }
  
  // return getAsync('rekon:' + fileName).then(fileExist => {
  //   if(fileExist) {
  //     return response.status(200).send({
  //       file: './public/' + fileName
  //     });
  //   }
  //   return response.status(200).send({
  //     file: null
  //   });
  // }).catch( () => {
  //   return response.status(200).send({
  //     file: null
  //   });
  // });  
});

module.exports = router;