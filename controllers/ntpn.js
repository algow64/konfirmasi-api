const express = require('express');
const validateJwt = require('../services/validate-jwt');
const ntpn = require('../services/ntpn');

const router = express.Router();

router.post('/', async (request, response) => {
  const requestPayloads = {
    jwt: request.headers.authorization,
    billings: request.body.billings.replace(/(\r\n|\n|\r)/gm, ""),
    userid: request.body.userid
  };

  const initNtpn = new ntpn(requestPayloads);
  const time = Date.now();
  const fileName = `rekon_${requestPayloads.userid}_${time}`;

  try {
    await validateJwt(requestPayloads.jwt);
  } catch (error) {
    return response.status(401).send({
      msg: 'unauthorized'
    });
  }

  initNtpn.fetchNtpn(fileName);

  return response.status(200).send({
    msg: 'success',
    file: fileName
  });
  
  // return validateJwt(requestPayloads.jwt).then(authStatus => {
  //   if(authStatus === 401) {
  //     return response.status(401).send({
  //       msg: 'unauthorized'
  //     });
  //   }
  //   return initNtpn.fetchNtpn();
  // }).then(res => {
  //   initNtpn.writeNtpn(res, fileName);
  // }).catch(() => {
  //   //
  // });
});

module.exports = router;