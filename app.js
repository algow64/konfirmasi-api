const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const controllers = require('./controllers/index');

const app = express();
const port = 4000;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/auth', controllers.auth);
app.use('/konfirmasi', controllers.ntpn);
app.use('/retrieve', controllers.retrieve);

app.listen(port, () => console.log(`Using port ${port}`));